# Mobwa.UsersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUser**](UsersApi.md#createUser) | **POST** /users | Create a user
[**getUser**](UsersApi.md#getUser) | **GET** /users/{userId} | Get user information
[**listUsers**](UsersApi.md#listUsers) | **GET** /users | List all users
[**updateUser**](UsersApi.md#updateUser) | **PUT** /users/{userId} | Update user information



## createUser

> [Transfer] createUser(opts)

Create a user

### Example

```javascript
import Mobwa from 'mobwa';
let defaultClient = Mobwa.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Mobwa.UsersApi();
let opts = {
  'createUserRequest': new Mobwa.CreateUserRequest() // CreateUserRequest | 
};
apiInstance.createUser(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createUserRequest** | [**CreateUserRequest**](CreateUserRequest.md)|  | [optional] 

### Return type

[**[Transfer]**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## getUser

> Account getUser(userId)

Get user information

### Example

```javascript
import Mobwa from 'mobwa';
let defaultClient = Mobwa.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Mobwa.UsersApi();
let userId = "userId_example"; // String | The id of the user to operate on
apiInstance.getUser(userId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **String**| The id of the user to operate on | 

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## listUsers

> [User] listUsers()

List all users

### Example

```javascript
import Mobwa from 'mobwa';
let defaultClient = Mobwa.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Mobwa.UsersApi();
apiInstance.listUsers().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[User]**](User.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateUser

> Account updateUser(userId, opts)

Update user information

### Example

```javascript
import Mobwa from 'mobwa';
let defaultClient = Mobwa.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Mobwa.UsersApi();
let userId = "userId_example"; // String | The id of the user to operate on
let opts = {
  'updateAccountRequest': new Mobwa.UpdateAccountRequest() // UpdateAccountRequest | 
};
apiInstance.updateUser(userId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **String**| The id of the user to operate on | 
 **updateAccountRequest** | [**UpdateAccountRequest**](UpdateAccountRequest.md)|  | [optional] 

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

