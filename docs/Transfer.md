# Mobwa.Transfer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**message** | **String** |  | [optional] 
**amount** | **Number** |  | [optional] 
**currency** | **String** |  | [optional] 
**createdAt** | **String** |  | [optional] 
**updatedAt** | **String** |  | [optional] 
**status** | **String** |  | [optional] 
**autoComplete** | **Boolean** |  | [optional] 
**source** | [**TransferSource**](TransferSource.md) |  | [optional] 
**destination** | [**TransferSource**](TransferSource.md) |  | [optional] 



## Enum: CurrencyEnum


* `SGD` (value: `"SGD"`)

* `USD` (value: `"USD"`)





## Enum: StatusEnum


* `PENDING` (value: `"PENDING"`)

* `SUCCEEDED` (value: `"SUCCEEDED"`)

* `FAILED` (value: `"FAILED"`)




