# Mobwa.TransfersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**completeTransfer**](TransfersApi.md#completeTransfer) | **POST** /accounts/{accountId}/transfers/{transferId}/complete | Complete a transfer
[**createTransfer**](TransfersApi.md#createTransfer) | **POST** /accounts/{accountId}/transfers | Create a transfer
[**deleteTransfer**](TransfersApi.md#deleteTransfer) | **DELETE** /accounts/{accountId}/transfers/{transferId} | Delete a transfer
[**getTransfer**](TransfersApi.md#getTransfer) | **GET** /accounts/{accountId}/transfers/{transferId} | Retrieve information for a specific transfer
[**listTransfers**](TransfersApi.md#listTransfers) | **GET** /accounts/{accountId}/transfers | List all transfers related to the account
[**updateTransfer**](TransfersApi.md#updateTransfer) | **PUT** /accounts/{accountId}/transfers/{transferId} | Change transfer information



## completeTransfer

> Transfer completeTransfer(accountId, transferId)

Complete a transfer

### Example

```javascript
import Mobwa from 'mobwa';
let defaultClient = Mobwa.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Mobwa.TransfersApi();
let accountId = "accountId_example"; // String | The id of the account
let transferId = "transferId_example"; // String | The id of the transfer to operate on
apiInstance.completeTransfer(accountId, transferId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account | 
 **transferId** | **String**| The id of the transfer to operate on | 

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## createTransfer

> Transfer createTransfer(accountId, opts)

Create a transfer

### Example

```javascript
import Mobwa from 'mobwa';
let defaultClient = Mobwa.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Mobwa.TransfersApi();
let accountId = "accountId_example"; // String | The id of the account
let opts = {
  'createTransferRequest': new Mobwa.CreateTransferRequest() // CreateTransferRequest | 
};
apiInstance.createTransfer(accountId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account | 
 **createTransferRequest** | [**CreateTransferRequest**](CreateTransferRequest.md)|  | [optional] 

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## deleteTransfer

> Transfer deleteTransfer(accountId, transferId)

Delete a transfer

### Example

```javascript
import Mobwa from 'mobwa';
let defaultClient = Mobwa.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Mobwa.TransfersApi();
let accountId = "accountId_example"; // String | The id of the account
let transferId = "transferId_example"; // String | The id of the transfers to operate on
apiInstance.deleteTransfer(accountId, transferId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account | 
 **transferId** | **String**| The id of the transfers to operate on | 

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getTransfer

> Transfer getTransfer(accountId, transferId)

Retrieve information for a specific transfer

### Example

```javascript
import Mobwa from 'mobwa';
let defaultClient = Mobwa.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Mobwa.TransfersApi();
let accountId = "accountId_example"; // String | The id of the account
let transferId = "transferId_example"; // String | The id of the transfers to operate on
apiInstance.getTransfer(accountId, transferId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account | 
 **transferId** | **String**| The id of the transfers to operate on | 

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## listTransfers

> [Transfer] listTransfers(accountId)

List all transfers related to the account

### Example

```javascript
import Mobwa from 'mobwa';
let defaultClient = Mobwa.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Mobwa.TransfersApi();
let accountId = "accountId_example"; // String | The id of the account
apiInstance.listTransfers(accountId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account | 

### Return type

[**[Transfer]**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateTransfer

> Transfer updateTransfer(accountId, transferId, opts)

Change transfer information

### Example

```javascript
import Mobwa from 'mobwa';
let defaultClient = Mobwa.ApiClient.instance;
// Configure HTTP basic authorization: basicAuth
let basicAuth = defaultClient.authentications['basicAuth'];
basicAuth.username = 'YOUR USERNAME';
basicAuth.password = 'YOUR PASSWORD';

let apiInstance = new Mobwa.TransfersApi();
let accountId = "accountId_example"; // String | The id of the account
let transferId = "transferId_example"; // String | The id of the transfers to operate on
let opts = {
  'updateTransferRequest': new Mobwa.UpdateTransferRequest() // UpdateTransferRequest | 
};
apiInstance.updateTransfer(accountId, transferId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**| The id of the account | 
 **transferId** | **String**| The id of the transfers to operate on | 
 **updateTransferRequest** | [**UpdateTransferRequest**](UpdateTransferRequest.md)|  | [optional] 

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

