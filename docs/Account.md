# Mobwa.Account

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**createdAt** | **String** |  | [optional] 
**currency** | **String** |  | [optional] 
**updatedAt** | **String** |  | [optional] 
**balance** | **Number** |  | [optional] 



## Enum: CurrencyEnum


* `USD` (value: `"USD"`)

* `SGD` (value: `"SGD"`)




