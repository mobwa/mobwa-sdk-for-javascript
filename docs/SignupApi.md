# Mobwa.SignupApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**signup**](SignupApi.md#signup) | **POST** /signup | Signup



## signup

> SignUpResponse signup(opts)

Signup

### Example

```javascript
import Mobwa from 'mobwa';

let apiInstance = new Mobwa.SignupApi();
let opts = {
  'signUpRequest': new Mobwa.SignUpRequest() // SignUpRequest | 
};
apiInstance.signup(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **signUpRequest** | [**SignUpRequest**](SignUpRequest.md)|  | [optional] 

### Return type

[**SignUpResponse**](SignUpResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

