/**
 * Mobwa Payments Hub
 * Mobwa Payments Hub allows anyone to transfer money between different accounts as well as manage them. It  currently supports the following currencies: United States Dollar (USD) and Singapura Dollar (SGD) 
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import Error from '../model/Error';
import SignUpRequest from '../model/SignUpRequest';
import SignUpResponse from '../model/SignUpResponse';

/**
* Signup service.
* @module mobwa/api/SignupApi
* @version 0.20210906.0
*/
export default class SignupApi {

    /**
    * Constructs a new SignupApi. 
    * @alias module:mobwa/api/SignupApi
    * @class
    * @param {module:mobwa/ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:mobwa/ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }



    /**
     * Signup
     * @param {Object} opts Optional parameters
     * @param {module:mobwa/model/SignUpRequest} opts.signUpRequest 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:mobwa/model/SignUpResponse} and HTTP response
     */
    signupWithHttpInfo(opts) {
      opts = opts || {};
      let postBody = opts['signUpRequest'];

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = SignUpResponse;
      return this.apiClient.callApi(
        '/signup', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Signup
     * @param {Object} opts Optional parameters
     * @param {module:mobwa/model/SignUpRequest} opts.signUpRequest 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:mobwa/model/SignUpResponse}
     */
    signup(opts) {
      return this.signupWithHttpInfo(opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


}
